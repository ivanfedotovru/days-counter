//
//  ViewController.swift
//  Days counter
//
//  Created by Иван on 06.02.17.
//  Copyright © 2017 Ivan Fedotov. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    // UPGRADE
    @IBOutlet weak var date: UIDatePicker!
    
    @IBAction func showStartDateDataPicker(_ sender: UITextField) {
        // data
    }

    // BASE
    
    //var item = Item?()
    var item: Item?
    
    @IBOutlet weak var nameOfCounter: UITextField!
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        let isInAddMode = presentingViewController is UINavigationController
        if isInAddMode {
            UIViewController().dismiss(animated: true, completion: nil)
        } else {
            navigationController!.popViewController(animated: true)
        }    
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let sender = sender as? UIBarButtonItem, saveButton === sender {
            let name = nameOfCounter.text ?? ""
            item = Item(name: name)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        if let item = item {
            nameOfCounter.text = item.name
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

