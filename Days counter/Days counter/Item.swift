//
//  Item.swift
//  Days counter
//
//  Created by Иван on 11.02.17.
//  Copyright © 2017 Ivan Fedotov. All rights reserved.
//

import Foundation

class Item: NSObject, NSCoding {
    var name: String
    
    static let Dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    
    static let ArchiveURL = Dir.appendingPathComponent("items")
    
    init?(name: String) {
        self.name = name
        super.init()
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: "name")
    }
    
    required convenience init? (coder aDecoder: NSCoder){
        let name = aDecoder.decodeObject(forKey: "name") as! String
        self.init(name: name)
    }
}
